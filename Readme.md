# Projeto Capstone SAV

## Objetivo

O objetivo deste projeto é apresentar o desenvolvimento do Projeto Capstone SAV até a semana 4.

## Exemplos

### Exemplo 1
* java Main t=n v="0,-1,3,1,10"

* Tipo: [Numérico]

* Valores: [0,-1,3,1,10]

* Ordenação: [-1, 0, 3, 1, 10]

Resultado:

<img src="./img/exemplo01.png" width="30%">


### Exemplo 2
* java Main t=c v="z,a,Z,A,b"

* Tipo: [Caractere]

* Valores: ['z','a','Z','A','b']

* Ordenação: [’A','Z’,’a’,'b’,’z’]

Resultado:

<img src="./img/exemplo02.png" width="30%">

### Exemplo 3

* java Main t=c v="z,1,Z,A,b"

* Tipo: [Caractere]

* Valores: ['z','1','Z','A','b']

* Valores Inválidos

Resultado:

<img src="./img/exemplo03.png" width="30%">


# Diagrama de classe
<img src="./img/diagramaClasse.png" width="30%">