package algoritmoOrdenacao;

import gui.Imprime;

public class BubbleSort extends Ordenacao {
	private Imprime imprime;

	@SuppressWarnings("static-access")
	public <T extends Comparable<T>> T[] sort(T[] array) {
		int n = array.length;
		T max = imprime.encontrarMaiorValor(array);
		boolean trocado;
		for (int i = 0; i < n; i++) {
			trocado = false;

			for (int j = 0; j < n - i - 1; j++) {
				if (array[j].compareTo(array[j + 1]) > 0) {
					T temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
					trocado = true;
					if (array[j] instanceof Integer) {
						imprime.imprimirAsteriscos(array, max);
					}
					/*
					 * EM PRODUÇÃO else { imprime.imprimirAsteriscosCHAR(array, max); }
					 * 
					 */
					imprime.imprimirValores(array);
					System.out.print("TROCA: " + array[j + 1] + " POR: " + array[j] + "\n\n");
					try {
						Thread.sleep(3000);
					} catch (Exception e) {
						Thread.currentThread().interrupt();
					}
				}
			}
			if (!trocado) {
				break;
			}
		}
		return array;
	}
}
