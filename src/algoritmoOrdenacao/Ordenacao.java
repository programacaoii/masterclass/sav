package algoritmoOrdenacao;

public abstract class Ordenacao {
	 public abstract <T extends Comparable<T>> T[] sort(T[] array);
	 
}
