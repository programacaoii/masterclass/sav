package entradaDados;

import gui.VerificaDigito;

public class EntradaDados<T> {
	private VerificaDigito verifica;

	@SuppressWarnings({ "unchecked", "static-access" })
	public T[] formatar(String args[], char c) {

		T[] vetorConvertido = null;

		if (args.length > 0) {
			String t = args[1].substring(2);
			String[] valores;

			// caso inteiro
			if (c == 'n') {
				valores = t.split(",");
				if (verifica.contemApenasNumeros(valores)) {
					vetorConvertido = (T[]) new Integer[valores.length];
					for (int i = 0; i < valores.length; i++) {
						Integer temp = Integer.parseInt(valores[i]);
						vetorConvertido[i] = (T) temp;
					}
				}

			}
			// caso seja caractere
			else if (c == 'c') {
				valores = t.split(",");
				if (verifica.contemApenasLetra(valores)) {
					vetorConvertido = (T[]) new Character[valores.length];
					for (int i = 0; i < valores.length; i++) {
						Character temp = valores[i].charAt(0);
						vetorConvertido[i] = (T) temp;
					}
				}
			}

		} else {
			System.out.println("Nenhum argumento passado.");
		}
		return vetorConvertido;
	}
}
