import entradaDados.EntradaDados;
import gui.Imprime;
import sav.Sav;

public class Main {

	@SuppressWarnings({ "rawtypes", "static-access" })
	public static <T> void main(String[] args) {

		Sav sav = new Sav();
		Imprime ip = new Imprime();
		EntradaDados formatacaoDados = new EntradaDados();
		String t = args[0].substring(2).toLowerCase();
		char c = t.charAt(0);

		switch (c) {

		case 'n':
			System.out.println("TIPO NUMERICO");
			Integer[] vetor = (Integer[]) formatacaoDados.formatar(args, c);
			System.out.print("Valores: ");

			if (vetor != null) {
				ip.imprimirValores(vetor);
				sav.executar(vetor);
				System.out.println("Vetor Ordenado:");
				ip.imprimirValores(vetor);
			}

			else {
				System.out.println("Entrada de dados Invalida");
			}
			break;

		case 'c':
			System.out.println("TIPO CARACTERE");
			Character[] caracter = (Character[]) formatacaoDados.formatar(args, c);
			System.out.print("Valores: ");

			if (caracter != null) {
				ip.imprimirValores(caracter);
				sav.executar(caracter);
				System.out.println("Vetor Ordenado:");
				ip.imprimirValores(caracter);
			}

			else {
				System.out.println("Entrada de dados Invalida");
			}

			break;

		default:
			System.out.println("Passagem de tipo errada");
		}

	}

}
