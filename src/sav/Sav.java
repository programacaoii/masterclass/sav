package sav;

import algoritmoOrdenacao.*;

public class Sav {
	private Ordenacao ordena;

	public Sav() {
		ordena = new BubbleSort();
	}

	public <T extends Comparable<T>> void executar(T[] array) {
		ordena.sort(array);

	}
}
