package gui;

public class VerificaDigito {

	public static boolean contemApenasNumeros(String[] array) {
	    for (String elemento : array) {
	        if (!eNumero(elemento)) {
	            return false;
	        }
	    }
	    return true;
	}

	private static boolean eNumero(String str) {
	    if (str.isEmpty()) {
	        return false;
	    }

	    if (str.charAt(0) == '-') {
	        str = str.substring(1);
	    }

	    for (char c : str.toCharArray()) {
	        if (!Character.isDigit(c)) {
	            return false;
	        }
	    }
	    return true;
	}
	
	public static boolean contemApenasLetra(String[]array) {
		for (String elemento : array) {
			char c = elemento.charAt(0);
			if (!(c >= 'A' && c <= 'Z') && !(c >= 'a' && c <= 'z')) {
				return false;
			}
		}
		return true;
	}


}
