package gui;

public class Imprime {

	
	public static <T extends Comparable<T>> T encontrarMaiorValor(T[] vetor) {
		T max = vetor[0];
		for (int i = 1; i < vetor.length; i++) {
			if (vetor[i].compareTo(max) > 0) {
				max = vetor[i];
			}
		}
		return max;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> void imprimirAsteriscos(T[] vetor, T max) {
		for (int i = (int) max - 1; i >= 0; i--) {
			for (int j = 0; j < vetor.length; j++) {
				if (((Comparable) i).compareTo(vetor[j]) < 0) {
					System.out.print("*\t");
				} else {
					System.out.print("\t");
				}
			}
			System.out.println();
		}

	}

	/* EM PRODUÇÃO
	public static <T> void imprimirAsteriscosCHAR(T[] vetor, T max) {

		Integer[] valor = new Integer[vetor.length];
		for (int j = 0; j < vetor.length; j++) {
			valor[j] = Character.getNumericValue((char) vetor[j]);
		}
		Integer maxNumber = Character.getNumericValue((char) max);

		imprimirAsteriscos(valor, maxNumber);

	}
	 */
	public static <T> void imprimirValores(T[] vetor) {
		for (T t : vetor) {
			System.out.print(t + "\t");
		}
		System.out.println("");
	}
}
